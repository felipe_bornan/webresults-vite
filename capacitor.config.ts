import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'net.bornan.kuwait2022',
  appName: 'Kuwait2022',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
