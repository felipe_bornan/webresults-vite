#!/bin/bash

VERSION="$(node bin/getCurrentVersion.js)"

function build {
    echo "Deleting previous v${VERSION}..."
    mkdir build
    rm -rf dist
    rm -rf build/${VERSION}
    rm -f build/current
    rm -f build/${VERSION}.tar.gz

    echo "Building v${VERSION}..."
    npm run build
    find ./dist -name '*.map' -delete
    echo "Compressing v${VERSION}..."
    cd dist
    tar cfz ../build/${VERSION}.tar.gz *
    cd ..
    echo "Finished production build!!!"
}

echo "you are going to create build v${VERSION}"
echo "** PLEASE CHECK THE VERSION LIKE 3 OR 10 TIMES **"
echo "do you want to continue? (y/n)"
read response
if [ "$response" = "y" ]; then
    echo "set ... ready ... action 🎬"
    build
else
    echo "cancelling ... ✋"
    exit 0
fi



