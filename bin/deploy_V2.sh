#!/bin/bash
# LA VARIABLE que se manda a deploy.sh es si es un deply de production o no
#

function deploy {
    SERVER=$1
    PRODUCT_NAME=$2
    VERSION=$3
    PRODUCTION=$4
    echo "Uploading version to: ${SERVER}"
    scp ${PROJECT_FOLDER}/${VERSION}.tar.gz ${SERVER}:~/workspace/${PRODUCT_NAME}/dist
    # copia el script de deploy-server
    scp bin/deploy-server-V2.sh ${SERVER}:~/workspace/${PRODUCT_NAME}
    echo "Connecting to: ${SERVER}"
    # el script deploy-server esta en el server, se ejecuta alli
    ssh ${SERVER} "cd workspace/${PRODUCT_NAME} && sh ./deploy-server-V2.sh ${VERSION} ${PRODUCTION}"
}
#
if [ -z "$1" ]
then
    PRODUCTION=false
    SERVER="$(node bin/getServerTestName.js)"
    echo "*** deploy to test (server:${SERVER}) ***"
else
    PRODUCTION=true
    SERVER="$(node bin/getServerProductionName.js)"
    echo "*** deploy to production (server:${SERVER}) ***"
fi
##
##
VERSION="$(node bin/getCurrentVersion.js)"
PROJECT_FOLDER="build"

PS3="Select a project (1-3): "
select p in jsj gcc exit
do
    case $p in
        jsj)
            PROJECT=jsj
            echo "** Rosario selected";;
        gcc)
           PROJECT=gcc
            echo "** Kuwait selected";;
        exit) exit;;
    esac
    PRODUCT_NAME="bornan-webresults3-${PROJECT}"
    VERSION_PROJECT="${VERSION}_${PROJECT}"

    if [ -z "$PROJECT" ]
    then
        echo "Error. No Project selected"
        exit 0
    else
        echo "Ready to deploy 🚀🚀" 
        echo "*** project: ${PRODUCT_NAME}"
        echo "*** version: ${VERSION_PROJECT}"
        echo "*** server: ${SERVER}"
        if [ "$PRODUCTION" = true ]; then
            echo "***"
            echo "*** P__R__O__D__U__C__T__I__O__N ***"
            echo "***"
        else
            echo "***"
            echo "*** T__E__S__T ***"
            echo "***"
        fi
        echo "do you want to continue? (y/n)"
        read response
        if [ "$response" = "y" ]; then
            echo "starting deploy 🎬"
            deploy "$SERVER" "$PRODUCT_NAME" "$VERSION_PROJECT" "$PRODUCTION"
        else
            echo "cancelling ... ✋"
            exit 0
        fi
    fi
    exit 0
done

