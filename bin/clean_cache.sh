#!/bin/bash
FOLDER="node_modules/.cache"

function cleancache {
    echo "Deleting webpack and vue cache"
    rm -rf ${FOLDER}
}

echo "clean Webpack cache ? (delete 'node_modules/.cache' folder)"
echo "do you want to continue? (y/n)"
read response
if [ "$response" = "y" ]; then
    echo "set ... ready ... action 🎬"
    cleancache
else
    echo "cancelling ... ✋"
    exit 0
fi


