#!/bin/bash
# LA VARIABLE que se manda a deploy.sh es si es un deply de production o no
if [ -z "$1" ]
then
    PRODUCTION=false
    SERVER="$(node bin/getServerTestName.js)"
    echo "*** deploy to test (server:${SERVER}) ***"
else
    PRODUCTION=true
    SERVER="$(node bin/getServerProductionName.js)"
    echo "*** deploy to production (server:${SERVER}) ***"
fi
##
##
VERSION="$(node bin/getCurrentVersion.js)"
PRODUCT_NAME="$(node bin/getProductName.js)"



function deploy_test {
    SERVER=$1
    PRODUCT_NAME=$2
    VERSION=$3
    echo "project ${PRODUCT_NAME} version ${VERSION}"
    echo "in server-test: ${SERVER}"
}

# function deploy_Ezequiel {
#     SERVER=$1
#     PRODUCT_NAME=$2
#     VERSION=$3
#     echo "Uploading version to: ${1}"
#     scp build/${VERSION}.tar.gz ${1}:~/workspace/${PRODUCT_NAME}
#     echo "Connecting to: ${1}"
#     ssh ${1} "cd workspace/${PRODUCT_NAME}"
#     echo "Removing old ${VERSION} files..."
#     rm -rf ./${VERSION}
#     mkdir ./${VERSION}

#     echo "Inflating ${VERSION} files..."
#     tar xfz ${VERSION}.tar.gz -C ${VERSION}

#     echo "Linking current folder..."
#     rm -f ./current
#     ln -s ./${VERSION} ./current
# }


function deploy {
    SERVER=$1
    PRODUCT_NAME=$2
    VERSION=$3
    PRODUCTION=$4
    echo "Uploading version to: ${SERVER}"
    scp build/${VERSION}.tar.gz ${SERVER}:~/workspace/${PRODUCT_NAME}/dist
    # copia el script de deploy-server
    scp bin/deploy-server.sh ${SERVER}:~/workspace/${PRODUCT_NAME}
    echo "Connecting to: ${SERVER}"
    # el script deploy-server esta en el server, se ejecuta alli
    ssh ${SERVER} "cd workspace/${PRODUCT_NAME} && sh ./deploy-server.sh ${VERSION} ${PRODUCTION}"
}

if [ -z "$PRODUCT_NAME" ] && [ -z "$SERVER" ]
then
    echo "Error. Cant read arguments from package.json"
    echo ""
    echo "VERSION, SERVER and PRODUCT_NAME is extracted from package.json"
    echo ""
    exit 0
else
    echo "Ready to deploy 🚀🚀" 
    echo "*** project: ${PRODUCT_NAME}"
    echo "*** version: ${VERSION}"
    echo "*** server: ${SERVER}"
    if [ "$PRODUCTION" = true ]; then
        echo "***"
        echo "*** P__R__O__D__U__C__T__I__O__N ***"
        echo "***"
    else
        echo "***"
        echo "*** T__E__S__T ***"
        echo "***"
    fi
    echo "do you want to continue? (y/n)"
    read response
    if [ "$response" = "y" ]; then
        echo "starting deploy 🎬"
        deploy "$SERVER" "$PRODUCT_NAME" "$VERSION" "$PRODUCTION"
    else
        echo "cancelling ... ✋"
        exit 0
    fi
fi