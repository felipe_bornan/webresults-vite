#!/bin/bash

DEFAULT_SERVER_FOLDER="bornan-webresult-front"
CURRENT_VERSION="$(node bin/getCurrentVersion.js)"

if [ $# -eq 0 ]; then
    echo "No arguments supplied. Usage:"
    echo ""
    echo "$ sh bin/deploy.sh SERVER_NAME SERVER_FOLDER (default is \"${DEFAULT_SERVER_FOLDER}\") VERSION (default is \"${CURRENT_VERSION}\" from source code)"
    echo ""
    exit 0
fi

SERVER_FOLDER=${2}
VERSION=${3}

if [[ -z "$2" ]]; then
    echo "Missing SERVER_FOLDER ${SERVER_FOLDER} -> default to ${DEFAULT_SERVER_FOLDER}"
    SERVER_FOLDER=${DEFAULT_SERVER_FOLDER}
fi

if [[ -z "$3" ]]; then
    echo "Missing VERSION -> default to CURRENT: ${CURRENT_VERSION}"
    VERSION=${CURRENT_VERSION}
fi

echo "Uploading version ${VERSION} to: ${1} in workspace/${SERVER_FOLDER}"
scp build/${VERSION}.tar.gz ${1}:~/workspace/${SERVER_FOLDER}/dist
echo "Connecting to: ${1}"
ssh ${1} "cd workspace/${SERVER_FOLDER}/dist
echo Removing old ${VERSION} files...
rm -rf ./${VERSION}
mkdir ./${VERSION}

echo Inflating ${VERSION} files...
tar xfz ${VERSION}.tar.gz -C ${VERSION}

echo Linking current folder...
rm -f ./current
ln -s ./${VERSION} ./current
"
