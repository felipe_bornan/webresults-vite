#!/bin/bash
VERSION="$(node bin/getCurrentVersion.js)"
PROJECT_FOLDER="build"

function build {
    echo "Deleting previous v${VERSION_PROJECT}..."
    mkdir ${PROJECT_FOLDER}
    rm -rf dist
    rm -rf ${PROJECT_FOLDER}/${VERSION_PROJECT}
    rm -f ${PROJECT_FOLDER}/current
    rm -f ${PROJECT_FOLDER}/${VERSION_PROJECT}.tar.gz

    echo "Building v${VERSION_PROJECT}..."
    #######
    npm run build
    #######
    find ./dist -name '*.map' -delete
    echo "Compressing v${VERSION_PROJECT}..."
    cd dist
    tar cfz ../${PROJECT_FOLDER}/${VERSION_PROJECT}.tar.gz *
    cd ..
    echo "Finished production build!!!"
}

PS3="Select a project (1-3): "
select p in jsj gcc exit
do
    case $p in
        jsj)
            PROJECT=jsj
            echo "** Rosario selected";;
        gcc)
           PROJECT=gcc
            echo "** Kuwait selected";;
        exit) exit;;
    esac
    VERSION_PROJECT="${VERSION}_${PROJECT}"
    #
    #
    if [ -z "$PROJECT" ]; then
        echo "No project selected"
        exit 0
    else
        echo "** project: ${PROJECT} **"
        echo "you are going to create build v${VERSION_PROJECT}"
        echo "** PLEASE CHECK THE VERSION AGAIN AND AGAIN **"
        echo "do you want to continue? (y/n)"
        read response
        if [ "$response" = "y" ]; then
            echo "set ... ready ... action 🎬"
            build
        else
            echo "cancelling ... ✋"
            exit 0
        fi
    fi
    exit 0
done


