#!/bin/bash
# este script esta copiado en el serve, se ejecuta alli
# esta es solo una copia
VERSION=$1
PRODUCTION=$2

if [ "$PRODUCTION" = true ]; then
    echo "** en produccion **"
else
    echo "** en test **"
fi

# Delete current symlink
echo "Removing old ${VERSION} files..."
rm -rf ${PWD}/dist/${VERSION}
mkdir ${PWD}/dist/${VERSION}

# Extract version
echo "Inflating ${VERSION} files..."
tar xfz "dist/${VERSION}.tar.gz" -C "dist/${VERSION}"

# Create current symlink
echo "Linking current folder..."
if [ "$PRODUCTION" = true ]; then
    rm -f ${PWD}/current
    ## en production, current no esta dentro de dist
    ln -s ${PWD}/dist/${VERSION} ${PWD}/current
    #echo "ln -s ${PWD}/dist/${VERSION} ${PWD}/current"
else
    rm -f ${PWD}/dist/current
    ln -s ${PWD}/dist/${VERSION} ${PWD}/dist/current
    #echo "ln -s ${PWD}/dist/${VERSION} ${PWD}/dist/current"
fi