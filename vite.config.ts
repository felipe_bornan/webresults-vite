import { defineConfig } from 'vite';
import { createVuePlugin as vue } from 'vite-plugin-vue2';
import path from 'path';
// import fs from 'fs';
// import moment from 'moment';

// THEME se lee del package.json y es lo que cambia los estilos para cada CHAMP
// const pkg = JSON.parse(fs.readFileSync('./package.json'));

// const theme = pkg.champ;
// console.log('Version', pkg.version);
// console.log('Champ:', pkg.champ, 'Route:', pkg.baseRoute);

// import.meta.env.VUE_APP_CHAMP = pkg.champ;
// import.meta.env.VUE_APP_ROUTE = pkg.baseRoute;
// import.meta.env.VUE_APP_VERSION = pkg.version;
// import.meta.env.VUE_APP_BUNDLED = moment().format('YYYY-MM-DDTHH:mm:00');

import config from './src/settings/config';
// const theme = config.general.champ;
const theme = 'JSJ22'; // tema de ROSARIO con datos de KUWAIT

// https://vitejs.dev/config/
const configDefinition: any = {
    plugins: [],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src'),
            '@theme': path.resolve(__dirname, `src/assets/${theme}`)
        }
    },
    css: {
        //postcss: {},
        preprocessorOptions: {
            scss: {
                charset: false, // no incluyas @charset en cada hoja de estilos, da error
                additionalData: '@use "sass:math"; @import "@theme/styles/base.scss";'
            }
        }
    },
    build: {
        target: ['esnext', 'safari12'], //funciona bien en Safari12, algunos errores de estilo
        // https://vitejs.dev/guide/build.html#library-mode
        cssCodeSplit: false, // If disabled, all CSS in the entire project will be extracted into a single CSS file.
        minify: false, // true with default esbuild
        assetsDir: 'lib',
        assetsInlineLimit: 4096, // Imported or referenced assets smaller than this will be inlined as base64 URLs to avoid extra http requests default 4096 (4kb)
        reportCompressedSize: true,
        chunkSizeWarningLimit: 600, // default 500 Kbs
        // https://rollupjs.org/guide/en/#big-list-of-options
        // NOT WORKING
        // lib: {
        //     entry: path.resolve(__dirname, 'main.ts'),
        //     name: 'MyLib',
        //     fileName: (format) => `my-lib.${format}.js`
        // },
        rollupOptions: {
            treeshake: 'recommended'
            // lib mode
            // external: ['vue'],
            // output: {
            //     // Provide global variables to use in the UMD build
            //     // for externalized deps
            //     globals: {
            //         vue: 'Vue'
            //     }
            // }
        }
    }
};

export default defineConfig(({ command, mode }) => {
    if (command === 'serve') {
        // con esta propiedad usa la configuracion aqui indicada, y no busca archivo de config
        configDefinition.css.postcss = {};
        configDefinition.plugins.push(vue());
        //configDefinition.server.port = 8080;
        return configDefinition;
    } else {
        // usa la configuracion en .postcssrc.js porque no existe option css.postcss
        //const vueOptions = { customElement: true };
        const vueOptions = {};
        configDefinition.plugins.push(vue(vueOptions));
        return configDefinition;
    }
});
