/* eslint-disable @typescript-eslint/no-var-requires */
const fs = require('fs');
const webpack = require('webpack');
const path = require('path');
//
// THEME se lee del package.json y es lo que cambia los estilos para cada CHAMP
const pkg = JSON.parse(fs.readFileSync('./package.json'));

const theme = pkg.champ;
console.log('Version', pkg.version);
console.log('Champ:', pkg.champ, 'Route:', pkg.baseRoute);

import.meta.env.VUE_APP_CHAMP = pkg.champ;
import.meta.env.VUE_APP_ROUTE = pkg.baseRoute;
import.meta.env.VUE_APP_VERSION = pkg.version;
import.meta.env.VUE_APP_BUNDLED = require('moment')().format('YYYY-MM-DDTHH:mm:00');

module.exports = {
    pages: {
        index: {
            // entry for the page
            entry: 'src/main.ts',
            // the source template
            template: `public/index_${theme}.html`,
            // output as dist/index.html
            filename: 'index.html'
        }
    },
    filenameHashing: true,
    productionSourceMap: false,
    css: {
        loaderOptions: {
            scss: {
                // esto funciona por el ALIAS, definido mas abajo
                prependData: '@use "sass:math"; @import "@theme/styles/base.scss";'
            }
        }
    },
    //
    configureWebpack: {
        plugins: [
            // exclude locales for moment
            new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en|es|fr|ar-kw/)
        ],
        resolve: {
            alias: {
                '@theme': path.resolve(__dirname, `src/assets/${theme}`)
            },
            extensions: ['.js', '.vue', '.json']
        }
    },
    chainWebpack: config => {
        // Disable splitChunks plugin, all the code goes into one bundle.
        config.optimization.splitChunks().clear();
    }
};
