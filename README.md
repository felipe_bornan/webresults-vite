# Bornan WebResults

## Project setup

### main branch

```
git switch main
```

### install node_modules

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

## Make annotated TAGS

By default , the Tag is made on the last commit

```
git tag -a v1.8.8 -m "web version 1.8.8"
```

Starting from Git 2.4 you can tell git (globally), that includes annotated tags when push (by default tags are not send in the push action)

```
git config --global push.followTags true
```

Now every push to remote, includes all commits and the annotated tags

## Checks before DEPLOY

-   Are you in **main** branch ?
-   The branch is clean ?
-   New web **version** in package.json
-   New app **version** in package.json
-   Check **settings/config.ts**, special atention to external and debug values
-   Restore normal **cache** behaviour in public/index.html
-   Check **comments**, specially in settings files
-   Check carefully the messages appearing on **build** and **deploy** scripts

## preview DIST

Vue sets the NODE_ENV value, to development, test or production

-   preview DIST with serve (npm i -g serve)

```
serve -s -p 8080 dist
```

## scripts for DEPLOY

-   **npm run bornan_build**

    -   [local] Delete previous builds and makes a new one, named after the version.
    -   [local] Build files are in dist, and compressed for distribution in build

-   **npm run bornan_deploy**
-   **npm run bornan_deploy_production**
    -   ** PRODUCTION ** version of the script, passes an argument to change the server and destination of the current symlink
    -   All variables are readed form **package.json**
    -   [server] Copy the compressed build onto the server
    -   [server] Copy the **deploy-server.sh** script also into the server (this way the script is always updated)
    -   [server] Connect to the server, cd to workspace folder and execute **deploy-server.sh** (script is in the server)
        -   [deploy-server.sh] Delete previous builds for the same version
        -   [deploy-server.sh] Inflate the build file
        -   [deploy-server.sh] Symbolic link for this version to **current** (Nginx serves always current)

### Mobile Apps

### Android

Before creating the applications for ios and android, the images corresponding to the icon and splash must be uploaded in the resources folder under the root directory of the app. With the following resolution each of them:

**resources/icon.png** -> 1024x1024 px
**resources/splash.png** -> 2732x2732 px

To create the .apk install CapacitorJS:

```
npm install @capacitor/cli @capacitor/core
```

Initialize the Capacitor instance in your project:

```
npx cap init
```

Add a platform to work with, for example android:

```
npm install @capacitor/android
npx cap add android
```

Build the VueJS application to build the web resources. As usual, the build will be stored in the dist directory of your Vue project and that's exactly what Capacitor will use as web resources. Synchronize your web assets with the Android project:

```
npx cap sync android
```

Install Android Studio from:

```
https://developer.android.com/studio?gclid=EAIaIQobChMIgtzfl-Gu9AIVvwUGAB2NUwR5EAAYASAAEgLKCvD_BwE&gclsrc=aw.ds
```

Assuming that you have already Android Studio installed, you may open the project with the following command:

```
npx cap open android
```

You can use then the UI of Android Studio to launch your new mobile app either in your own device or an emulator.

For the generation of the icons we must have installed capacitor-resources:

```
npm install capacitor-resources -g
```

**First step**, create the ios and android projects:

```
npm run android
```

```
npm run ios
```

**Second step**, create the icons in the generated projects:

```
npm run resources
```

Now, the corresponding applications can be run

```
npm run run-android
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
