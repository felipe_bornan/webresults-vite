import { Component, Vue, Watch } from 'vue-property-decorator';
import titleUnit from '@/components/DisciplinesMatchCentre/_commons/Title.vue';

/* eslint-disable */
@Component({
    props: ['results'],
    components: { titleUnit }
})
/* eslint-enable */
export default class ATHStartList extends Vue {
    unitInfo: any = null;
    type = '';

    @Watch('results')
    setValues(value: any, old?: any) {
        if (!value) {
            this.unitInfo = null;
            this.type = '';
            return;
        }
        this.unitInfo = value.Info;
        this.type = value.Info.Type ? value.Info.Type : 'T';
    }

    mounted() {
        this.setValues(this.$props.results);
    }
}
