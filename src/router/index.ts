import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Main from '@/views/Main.vue';
import config from '@/settings/config';
const baseRoute = config.general.baseRoute;

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
    { path: '/', redirect: `/${baseRoute}/schedule/disciplines` },
    {
        name: 'games_page',
        path: '/awbgqatar.com',
        beforeEnter() {
            location.href = `/${baseRoute}/schedule/disciplines`;
        }
    },
    {
        name: 'sal',
        path: `/${baseRoute}`,
        component: Main,
        redirect: `/${baseRoute}/schedule/disciplines`,
        children: [
            {
                name: 'schedule',
                path: 'schedule/:tab?/:day?',
                component: () => import('@/views/Schedule.vue')
            },
            {
                name: 'athletes',
                path: 'participants/:type?',
                component: () => import('@/views/Participants.vue')
            },
            {
                name: 'med_standings',
                path: 'medals/standings/:disc?',
                component: () => import('@/views/Medals.vue')
            },
            {
                name: 'med_search',
                path: 'medals/search/:disc?/:noc?/:metal?/:gender?',
                component: () => import('@/components/Medals/SearchMedals.vue')
            },
            {
                name: 'reports',
                path: 'reports',
                component: () => import('@/views/Reports.vue')
            },
            {
                name: 'countries',
                path: 'nocs/:noc?',
                component: () => import('@/views/Nocs.vue')
            },
            {
                name: 'teamsbio',
                path: 'athletes/:disc/:reg',
                component: () => import('@/views/Athlete.vue')
            },
            {
                name: 'athletesbio',
                path: 'athletes/:disc/:reg',
                component: () => import('@/views/Athlete.vue')
            },
            {
                name: 'discipline',
                path: 'discipline/:disc',
                redirect: 'discipline/:disc/schedule',
                component: () => import('@/views/Discipline.vue'),
                children: [
                    {
                        name: 'disc_schedule',
                        path: 'schedule/:date?',
                        component: () => import('@/components/Disciplines/Schedule.vue')
                    },
                    {
                        name: 'disc_results',
                        path: 'results/:rsc?',
                        component: () => import('@/components/Disciplines/Results.vue')
                    },
                    {
                        name: 'disc_entries',
                        path: 'entries/:rsc?',
                        component: () => import('@/components/Disciplines/Entries.vue')
                    },
                    {
                        name: 'disc_reports',
                        path: 'reports',
                        component: () => import('@/views/Reports.vue')
                    },
                    {
                        name: 'disc_medals',
                        path: 'medals',
                        component: () => import('@/components/Disciplines/Medals.vue')
                    },
                    {
                        name: 'disc_brackets',
                        path: 'knock-out/:rsc?',
                        component: () => import('@/components/Disciplines/Brackets.vue')
                    },
                    {
                        name: 'disc_groups',
                        path: 'groups/:rsc?/:group?',
                        component: () => import('@/components/Disciplines/Groups.vue')
                    },
                    {
                        name: 'disc_frank',
                        path: 'final-rank/:rsc?',
                        component: () => import('@/components/Disciplines/FinalRank.vue')
                    },
                    {
                        name: 'disc_comms',
                        path: 'communications/:id?',
                        component: () => import('@/components/Disciplines/Communications.vue')
                    }
                ]
            }
        ]
    },
    {
        name: 'notFound',
        path: '*',
        component: () => import('@/views/404.vue')
    }
];

const router = new VueRouter({
    routes
});

export default router;
