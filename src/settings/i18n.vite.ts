import Vue from 'vue';
import VueI18n, { LocaleMessages } from 'vue-i18n';
import store from '../store';

Vue.use(VueI18n);

const messages: { [lang: string]: any } = {};
const modules = import.meta.globEager('../locales/*.json');
// console.log(modules);
['en', 'es'].forEach(locale => {
    messages[locale] = modules[`../locales/${locale}.json`];
});
// console.log(messages);

const localLang = store.getters.language;
const i18n = new VueI18n({
    locale: localLang || import.meta.env.VUE_APP_I18N_LOCALE || 'en',
    fallbackLocale: import.meta.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en',
    messages: messages,
    silentTranslationWarn: true
});
export default i18n;
