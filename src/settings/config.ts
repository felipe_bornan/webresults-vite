export default {
    copyrightYear: '2022',
    analytics: {
        enable: false,
        debug: false,
        id: 'UA-118365026-1'
    },
    general: {
        //champ: 'GCC2020',
        // baseRoute: 'kuwait2022',
        champ: 'JSJ22',
        baseRoute: 'rosario2022',
        //
        champDesc: { L1: '', S1: '', L2: '', S2: '', L3: '', S3: '' },
        storage: '', //
        venueTimeZone: '', // '+03:00'
        photoPath: '', // viene del backend
        useSockets: true,
        socketsURL: '',
        // Services URL data
        services: {
            // localhost: 'http://localhost:3000',
            localold: 'https://bornan-webresults-back.bornan.net',
            local: 'https://back.v3.webresults.bornan.net',
            debug: true,
            // externalTEST: 'https://bornan-webresults-back.bornan.net',
            // PRODUCTION: debug is always false in production
            externalold: 'https://back.kuwait2022.bornan.net',
            external: 'https://back.v3.webresults.bornan.net'
        }
    },
    // Results refresh timeouts in seconds!!
    timeouts: {
        config: 10 * 60,
        schedule: 60,
        sports: {}
    }
};
