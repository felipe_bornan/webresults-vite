'use strict';

const rscDisc: any = {};
const schDisc: any = {};
const medals: any = {};
const partics: any = {};

function concat(origin: any[], toAdd: any[]) {
    for (let i = 0; i < toAdd.length; i++) origin.push(toAdd[i]);
}

function retrieveFilter(obj: any, name: string): any[] {
    if (obj[name]) return obj[name];
    return [];
}

function saveFilter(obj: any, name: string, filters: any[]) {
    const copy = filters.slice();
    obj[name] = copy;
}

function setRSC(disc: string, rsc: string) {
    rscDisc[disc] = rsc;
}

function getRSC(disc: string) {
    if (rscDisc[disc]) return rscDisc[disc];
    return null;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

function setFilterPartics(selNOCs: any[], selDisc: any[]) {
    saveFilter(partics, 'nocs', selNOCs);
    saveFilter(partics, 'disc', selDisc);
}

function getFilterPartics(selNOCs: any[], selDisc: any[]) {
    selNOCs.splice(0);
    selDisc.splice(0);
    //
    const savedNOCs = retrieveFilter(partics, 'nocs');
    const savedDisc = retrieveFilter(partics, 'disc');
    concat(selNOCs, savedNOCs);
    concat(selDisc, savedDisc);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

function setFilterMedals(selDays: any[], selNOCs: any[], selDisc: any[]) {
    saveFilter(medals, 'days', selDays);
    saveFilter(medals, 'nocs', selNOCs);
    saveFilter(medals, 'disc', selDisc);
}

function getFilterMedals(selDays: any[], selNOCs: any[], selDisc: any[]) {
    selDays.splice(0);
    selNOCs.splice(0);
    selDisc.splice(0);
    //
    const savedDays = retrieveFilter(medals, 'days');
    const savedNOCs = retrieveFilter(medals, 'nocs');
    const savedDisc = retrieveFilter(medals, 'disc');
    concat(selDays, savedDays);
    concat(selNOCs, savedNOCs);
    concat(selDisc, savedDisc);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

function setFilterSch(disc: string, selEvt: any[], selNOC: any[], selATH: any[], selStt: any[], selDay: any[]) {
    if (!disc) return;
    saveFilter(schDisc, disc + '.evts', selEvt);
    saveFilter(schDisc, disc + '.nocs', selNOC);
    saveFilter(schDisc, disc + '.aths', selATH);
    saveFilter(schDisc, disc + '.stts', selStt);
    saveFilter(schDisc, disc + '.days', selDay);
}

function getFilterSch(disc: string, selEvt: any[], selNOC: any[], selATH: any[], selStt: any[], selDay: any[]) {
    selEvt.splice(0);
    selNOC.splice(0);
    selATH.splice(0);
    selStt.splice(0);
    selDay.splice(0);
    //
    const savedEvt = retrieveFilter(schDisc, disc + '.evts');
    const savedNOC = retrieveFilter(schDisc, disc + '.nocs');
    const savedATH = retrieveFilter(schDisc, disc + '.aths');
    const savedStt = retrieveFilter(schDisc, disc + '.stts');
    const savedDay = retrieveFilter(schDisc, disc + '.days');
    concat(selEvt, savedEvt);
    concat(selNOC, savedNOC);
    concat(selATH, savedATH);
    concat(selStt, savedStt);
    concat(selDay, savedDay);
}

export default {
    setRSC,
    getRSC,
    //
    setFilterPartics,
    getFilterPartics,
    //
    setFilterMedals,
    getFilterMedals,
    //
    setFilterSch,
    getFilterSch
};
