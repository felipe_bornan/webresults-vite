'use scrict';
import config from './config';
import axios from 'axios';
import pako from 'pako';

import appStore from '../store';

let cancelTkn = axios.CancelToken.source();
const champ = config.general.champ;

function defaultDisc(disc: string | null | undefined): string {
    return disc ? disc : 'ALL';
}

function sPath(path: string, disc?: string | null, param1?: string, param2?: string, param3?: string) {
    const lang = appStore.getters.language;
    disc = defaultDisc(disc);

    switch (path) {
        case 'config':
            return `/s/${champ}/${lang}/config`;
        // Medals
        case 'medal_standings':
            return `/s/${champ}/${lang}/${disc}/medals/standings`;
        //case 'medal_multi'          :   return `/s/${champ}/${lang}/ALL/medals/multi-medallists`
        case 'medal_params':
            return `/s/${champ}/${lang}/ALL/medals/params`;
        case 'medal_daily':
            return `/s/${champ}/${lang}/ALL/medals/daily/${param1}`;
        case 'medal_latest':
            return `/s/${champ}/${lang}/${disc}/medals/latest`;
        case 'medal_discipline':
            return `/s/${champ}/${lang}/${disc}/medals/discipline`;
        // Medals NOC
        case 'medal_noc':
            return `/s/${champ}/${lang}/${disc}/medals/org/${param1}`;
        //case 'medal_noc_gender'     :   return `/s/${champ}/${lang}/${disc}/medals/noc/${param1}/gender/${param2}`
        //case 'medal_noc_medal'      :   return `/s/${champ}/${lang}/${disc}/medals/noc/${param1}/${param2}`
        //case 'medal_noc_medal_g'    :   return `/s/${champ}/${lang}/${disc}/medals/noc/${param1}/${param2}/gender/${param3}`

        // Entries
        case 'nocs':
            return `/s/${champ}/${lang}/ALL/orgs/list`;
        case 'nocs_info':
            return `/s/${champ}/${lang}/ALL/entries/orgs/info`;
        case 'participants':
            return `/s/${champ}/${lang}/${disc}/entries/list`;
        case 'disciplines':
            return `/s/${champ}/${lang}/ALL/disc/list`;
        case 'biography':
            return `/s/${champ}/${lang}/${disc}/entries/bio/${param1}`;
        case 'disc_data':
            return `/s/${champ}/${lang}/${disc}/disc/data`;
        case 'disc_data_event':
            return `/s/${champ}/${lang}/${disc}/data/event/${param1}`;
        case 'disc_events':
            return `/s/${champ}/${lang}/${disc}/events`;
        case 'disc_events_ph':
            return `/s/${champ}/${lang}/${disc}/events/phases`;
        case 'disc_events_ph_un':
            return `/s/${champ}/${lang}/${disc}/events/phases/units`;
        case 'entries_disc':
            return `/s/${champ}/${lang}/${disc}/entries`;
        case 'entries_noc':
            return `/s/${champ}/${lang}/${disc}/entries/noc/${param1}`;
        case 'entries_event':
            return `/s/${champ}/${lang}/${disc}/entries/event/${param1}`;

        // Schedule
        case 'schedule':
            return `/s/${champ}/${lang}/ALL/schedule/matrix`;
        //case 'schedule_live_disc'   :   return `/s/${champ}/${lang}/ALL/schedule/live-disc`
        case 'schedule_days':
            return `/s/${champ}/${lang}/${disc}/schedule/days`;
        case 'schedule_date':
            // championship:discipline:day:units[]
            return `/s/${champ}/${lang}/${disc}/schedule/daily/${param1}`;
        case 'schedule_event':
            // championship:discipline:event:units[]
            return `/s/${champ}/${lang}/${disc}/schedule/event/${param1}`;
        case 'schedule_landing':
            // championship:discipline:{ last:units[], live:units[], next:units[] }
            return `/s/${champ}/${lang}/${disc}/schedule/landing`;

        // Results
        case 'res_results':
            return `/s/${champ}/${lang}/${disc}/results/${param1}`;
        case 'res_all_around':
            return `/s/${champ}/${lang}/${disc}/results/${param1}/all-around`;
        case 'res_groups':
            return `/s/${champ}/${lang}/${disc}/groups/${param1}`;
        case 'res_brackets':
            return `/s/${champ}/${lang}/${disc}/brackets/${param1}`;
        case 'res_final-rank':
            return `/s/${champ}/${lang}/${disc}/final-rank/${param1}`;
        case 'res_phases':
            return `/s/${champ}/${lang}/${disc}/phases/${param1}`;

        // Comms
        case 'comms_list':
            return `/s/${champ}/${lang}/${disc}/communications`;
        case 'comms_detail':
            return `/s/${champ}/${lang}/${disc}/communication/${param1}`;

        // Reports
        case 'reports_all':
            return `/s/${champ}/${lang}/${disc}/reports/all`;
        case 'reports_disc':
            return `/s/${champ}/${lang}/${disc}/reports/disc`;
        case 'reports_event':
            return `/s/${champ}/${lang}/${disc}/reports/event/${param1}`;
        case 'reports_unit':
            return `/s/${champ}/${lang}/${disc}/reports/unit/${param1}`;
        case 'reports_books':
            return `/s/${champ}/${lang}/${disc}/reports/result-book`;

        // Records
        case 'records_initial':
            return `/s/${champ}/${lang}/${disc}/records/initial`;
        case 'records_broken':
            return `/s/${champ}/${lang}/${disc}/records/broken`;
    }
    return '/not-found';
}

const methods = {
    async link(
        path: string,
        disc?: string | null,
        param1?: string,
        param2?: string,
        param3?: string
    ): Promise<any | null> {
        let isDebug = false;
        let sURL = sPath(path, disc, param1, param2, param3);
        if (import.meta.env.NODE_ENV === 'production') {
            // en production, nunca estará en debug mode
            sURL = config.general.services.external + sURL;
        } else {
            isDebug = config.general.services.debug === true;
            if (isDebug) {
                sURL = '/debug' + sURL;
            }
            sURL = config.general.services.local + sURL;
        }

        try {
            const resp = await axios.get(sURL, { cancelToken: cancelTkn.token });
            const data = resp.data;
            if (isDebug) return data;
            // Descomprimo usando 'pako'
            const jsonStr = pako.inflate(data, { to: 'string' });
            //console.log(jsonStr);
            return JSON.parse(jsonStr);
        } catch (error) {
            if (!axios.isCancel(error)) {
                //console.error('Service error:', error.message)
            }
            return null;
        }
    },
    async downloadPdf(newLink: string, namePdf: string) {
        try {
            const resp = await axios.get(newLink, { responseType: 'blob' });
            const url = window.URL.createObjectURL(new Blob([resp.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', namePdf + '.pdf');
            document.body.appendChild(link);
            link.click();
        } catch (error) {
            console.error('Download PDF', (error as any).message);
        }
    },
    cancelRequests() {
        cancelTkn.cancel('');
        cancelTkn = axios.CancelToken.source();
    }
};

export default methods;
