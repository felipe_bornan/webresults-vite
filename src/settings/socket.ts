import io from 'socket.io-client';
import pako from 'pako';
//
import config from './config';
import store from '../store';

let socket: SocketIOClient.Socket | null = null;
let handlers: any = {};
let rooms: any[] = [];

//
//
function addHandler(room: string, fn: (err: boolean, data: any) => void) {
    handlers[room] = fn;
    rooms.push(room);
}
function delHandler(room: string) {
    handlers[room] = null;
    const index: number = rooms.indexOf(room);
    if (index >= 0) rooms.splice(index, 1);
}

function delHandlers() {
    handlers = {};
}

function connect() {
    if (socket != null) {
        return;
    }

    socket = io(config.general.socketsURL);

    socket.on('connect', () => {
        if (socket == null) return;
    });
    socket.on('reconnect_attempt', (attemptNumber: number) => {
        if (socket == null) return;
        console.log('attempt', attemptNumber, new Date().toISOString());
        if (attemptNumber == 10) {
            for (const room in handlers) {
                const fn = handlers[room];
                fn(true, null);
            }
            disconnect();
        }
    });
    socket.on('reconnect', (att: number) => {
        console.log('Reconnected after ' + att + ' attempts.');
        if (socket == null) return;
        for (const room in handlers) {
            socket.emit('join', room);
        }
    });
    socket.on('data', function(data: any) {
        const room = data.room;
        console.log(room);
        if (room && handlers[room]) {
            const fn = handlers[room];
            const jsonStr = pako.inflate(data.info, { to: 'string' });
            const restored = JSON.parse(jsonStr);
            fn(false, restored);
        }
    });
}
function disconnect() {
    if (socket != null) {
        socket.disconnect();
        console.log('Socket disconected', socket.disconnected);
        socket = null;
    }
}

export default {
    roomName(type: string, disc: string, rsc: string): string {
        const lang = store.state.language;
        if (type == 'results') {
            return `${config.general.champ}.${lang}.res.${disc}.${rsc}`;
        }
        return `${config.general.champ}.${lang}.${type}.${disc}.${rsc}`;
    },

    join(room: string, fn: (err: boolean, data: any) => void) {
        if (config.general.useSockets == true) {
            connect();
            if (socket != null) {
                socket.emit('join', room);
                addHandler(room, fn);
            }
        } else {
            fn(true, null);
        }
    },

    leave(room: string) {
        if (socket == null) return;
        socket.emit('leave', room);
        delHandler(room);
        if (!rooms.length) disconnect();
    },
    leaveAll() {
        if (socket == null) return;
        delHandlers();
        socket.emit('leaveAll');
        rooms = [];
        disconnect();
    }
};
