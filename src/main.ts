console.log(new Date().toISOString(), 'start');
import Vue from 'vue';
import VueAnalytics from 'vue-analytics';
// https://vue-scrollto.netlify.com/docs/#as-a-vue-directive
import VueScrollTo from 'vue-scrollto';
//
import router from './router';
import store from './store';
import i18n from './settings/i18n';
import config from './settings/config';
//
import App from './App.vue';
//bootstrap-vue
import { CollapsePlugin } from 'bootstrap-vue';
//
const champ = config.general.champ;
// componentes especificos del theme = champ
const champComponents = import(`./assets/${champ}/components/components`);
//
Vue.use(CollapsePlugin);

Vue.use(VueScrollTo);
Vue.config.productionTip = false;

if (config.analytics.enable) {
    Vue.use(VueAnalytics, {
        id: config.analytics.id,
        router,
        debug: {
            enabled: config.analytics.debug,
            sendHitTask: !config.analytics.debug
        }
    });
}

router.afterEach((to, from) => {
    const scrolling = document.scrollingElement;
    // casi siempre el elmento que hace scroll es el HTML == document.documenElement
    // funciona incluso el Safari 12.1 del iPhone6
    if (window) {
        setTimeout(() => {
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }, 100);
    }
    // en iOS no existe este elemento ? auqnue el scroll funciona
    if (scrolling) {
        setTimeout(() => {
            scrolling.scrollTop = 0;
        }, 200);
    }
    // setTimeout(() => {
    //     const html = document.documentElement as HTMLElement;
    //     const box = html.getBoundingClientRect();
    //     console.log('html elemento values:');
    //     console.log(box);
    //     console.log(
    //         'scrollTop value = ',
    //         html.scrollHeight,
    //         '-',
    //         window.innerHeight,
    //         ' = ',
    //         html.scrollHeight - window.innerHeight
    //     );
    // }, 500);
});

async function loadComponents() {
    const toRegister = (await champComponents).default;
    const { components, discComponents } = toRegister;
    for (const key in components) {
        Vue.component(key, components[key]);
    }
    for (const key in discComponents) {
        Vue.component(key, (await discComponents[key]).default);
    }
    // console.log(components, discComps);
    // const promiseComps = champComponents
    //     .then(promiseComps => {
    //         // console.log(promiseComps);
    //         promiseComps.default.then((comps: { [key: string]: any }[]) => {
    //             // console.log('in components: ', comps);
    //             Object.entries(comps).forEach(([key, component]) => {
    //                 // console.log('REGISTERED COMPONENT: ' + key);
    //                 if (component.default) {
    //                     // componentes importados dinamicamente
    //                     Vue.component(key as string, component.default);
    //                 } else {
    //                     // componentes importados normalmente
    //                     Vue.component(key as string, component);
    //                 }
    //             });
    //         });
    //     })
    //     .catch(error => {
    //         console.log(error);
    //     });
    console.log(new Date().toISOString(), 'creating app');
    new Vue({
        router,
        store,
        i18n,
        render: h => h(App)
    }).$mount('#app');
}

loadComponents();
