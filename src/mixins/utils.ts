'use strict';

import { Vue } from 'vue-property-decorator';

export default Vue.extend({
    methods: {
        removeDuplicatesByObject(objKey: any, array: any) {
            const trimmedArray: any = [];
            const values: any = [];
            let value: any;
            for (let i = 0; i < array.length; i++) {
                value = array[i][objKey];
                if (values.indexOf(value) === -1) {
                    trimmedArray.push(array[i]);
                    values.push(value);
                }
            }
            return trimmedArray;
        },
        async removeDuplicatesByProp(array: any, prop: any) {
            const obj: any = {};
            return await Object.keys(
                array.reduce((prev: any, next: any) => {
                    if (!obj[next[prop]]) obj[next[prop]] = next;
                    return obj;
                }, obj)
            ).map(i => obj[i]);
        },
        groupBy(list: any, props: any) {
            return list.reduce((a: any, b: any) => {
                (a[b[props]] = a[b[props]] || []).push(b);
                return a;
            }, {});
        },
        isObjectEmpty(obj: any) {
            for (const key in obj) {
                if (obj[key]) return false;
            }
            return true;
        },
        checkParam(collection: any, value: any) {
            for (let i = 0; i < collection.length; i++) {
                if (collection[i].Key == value) return true;
            }
            return false;
        },

        getTitleDesc(disc: string) {
            const arrTitle = this.$store.state.disciplines.filter((e: any) => e.Key === disc);

            return arrTitle[0] ? arrTitle[0].Desc : 'Discipline name';
        },
        getEventDescFromKey(key: any, eventDisc: any) {
            const eventsSelected = eventDisc.find((even: any) => {
                return even.EvKey === key;
            });
            return eventsSelected.Desc;
        },
        goBio(disc: string, reg: string) {
            this.$router.push({ name: 'athletesbio', params: { disc: disc, reg: reg } });
        }
    }
});
