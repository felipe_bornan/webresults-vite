'use strict';

import { Vue } from 'vue-property-decorator';
import moment from 'moment';

export default Vue.extend({
    methods: {
        formatDate(st: string | undefined, format: string): string {
            if (!st) return '';
            const mm = moment(st);
            const myTimeZone: boolean = this.$store.getters.myTime;
            // 2018-04-29T13:00:00+02:00
            if (st.length == 25 && !myTimeZone) {
                // Mantengo la zona horaria original
                const tz = st.substring(19);
                const result = mm.utcOffset(tz).format(format);
                return result;
            }
            return mm.format(format);
        },

        getHours(date: any) {
            return this.formatDate(date, 'LT');
        },
        getHoursMinutes(date: any) {
            return this.formatDate(date, 'HH:mm');
        },
        getDays(date: any) {
            return this.formatDate(date, 'DD');
        },
        getMonths(date: any) {
            return this.formatDate(date, 'MMM');
        },
        getDayMonth(date: any) {
            return this.formatDate(date, 'DD MMM');
        },
        getWeekDay(date: any, showTime: boolean) {
            if (showTime) return this.formatDate(date, 'ddd DD, HH:mm');
            else return this.formatDate(date, 'ddd DD');
        },
        getWeekDayMonth(date: any) {
            return this.formatDate(date, 'ddd, DD MMM');
        },
        getYearMonthDay(date: any) {
            return moment(date).format('YYYY-MM-DD');
        }
    }
});
