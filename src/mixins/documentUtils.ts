//import { Component, Vue } from 'vue-property-decorator';
import Vue from 'vue';

export default Vue.extend({
    name: 'DocumentMixin',
    methods: {
        scrollToTop() {
            const root = document.documentElement;
            root.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        },

        // https://codepen.io/Mamboleoo/post/scrollbars-and-css-custom-properties

        getScrollbarSize() {
            // el elemento que genera el scroll, suele ser el root
            const scroller1 = document.scrollingElement as any; // es el elemento html, pero este no tiene la scrollbar ??
            const scroller = document.body;
            // Force scrollbars to display, NOOOO
            //scroller.style.setProperty('overflow', 'scroll');
            // window.innerWidth, includes the width of the vertical scroll barm if its present
            // scroller.clientWidth, inner width of the element in pixels, includes padding, but excludes, borders, margins and vertical scroll bar, if present
            // console.log(
            //     scroller,
            //     document.documentElement.clientWidth,
            //     scroller1.clientWidth,
            //     window.innerWidth,
            //     document.body.clientWidth,
            //     document.body.scrollWidth
            // );
            //return window.innerWidth - scroller.clientWidth;
            //return document.body.clientWidth - document.body.scrollWidth;
            return window.innerWidth - document.documentElement.clientWidth;
        },

        setScrollbarSize() {
            const scrollbarWidth = this.getScrollbarSize();
            //console.log('scrollbar width: ' + scrollbarWidth);
            document.documentElement.style.setProperty('--scrollbar', `${scrollbarWidth}px`);
            //console.log(document.documentElement.style.getPropertyValue('--scrollbar'));
        },

        checkOverflowers() {
            document.querySelectorAll('*').forEach(el => {
                // si son iguales, el contenido del elemento, cabe sin necesidad de scroll horizontal
                if (el.clientWidth == el.scrollWidth) {
                    //console.log('its ok');
                } else {
                    if (el.clientWidth > document.documentElement.clientWidth) {
                        console.log('Found the worst element ever: ', el);
                    }
                }
            });
        },

        searchOverflow() {
            const elmMain = document.querySelectorAll('#app')[0] as HTMLElement;
            const refElm = document.querySelectorAll('#app .main main .content')[0] as HTMLElement;
            const allElms = document.querySelectorAll('#app .main main .content *') as NodeListOf<HTMLElement>;
            // console.log(allElms);
            allElms.forEach(htmlel => {
                if (htmlel.clientWidth > elmMain.clientWidth) {
                    // escode el elemento y todos los de la misma clase
                    const sameClass = document.getElementsByClassName(htmlel.classList.value) as HTMLCollectionOf<
                        HTMLElement
                    >;
                    if (sameClass.length > 1) {
                        for (let i = 0; i < sameClass.length; i++) {
                            //console.log('ocultando ' + el.classList.value + ', son ' + sameClass.length);
                            (sameClass[i] as HTMLElement).style.setProperty('display', 'none');
                        }
                        if (refElm.clientWidth <= elmMain.clientWidth) {
                            console.log('CULPABLE ref:' + refElm.clientWidth + ', main:' + elmMain.clientWidth);
                            for (let i = 0; i < sameClass.length; i++) {
                                sameClass[i].style.removeProperty('display');
                                sameClass[i].style.setProperty('border', '3px solid red');
                            }
                        } else {
                            for (let i = 0; i < sameClass.length; i++) {
                                sameClass[i].style.removeProperty('display');
                                //sameClass[i].style.setProperty('border', '1px solid green');
                            }
                        }
                    } else {
                        //const htmlEl = el as HTMLElement;
                        htmlel.style.setProperty('display', 'none');

                        if (refElm.clientWidth <= elmMain.clientWidth) {
                            console.log('es sospechoso');
                            htmlel.style.removeProperty('display');
                            htmlel.style.setProperty('border', '1px solid red');
                            //console.log('elm:' + el.clientWidth + ' vs ref: ' + elmMain.clientWidth);
                            //console.log(el);
                            //console.log('Found the worst element ever: ', el);
                        } else {
                            htmlel.style.removeProperty('display');
                            //el.style.setProperty('border', '1px solid green');
                        }
                    }
                }
            });
            return 'done';
        }
    }
});

// @Component
// export default class DocumentMixin extends Vue {
//     // function scrollToTopv1() {
//     //     //Scroll to top
//     //     window.scrollTo(0, 0);
//     //     // const p = document.querySelectorAll('#fill');
//     //     // for (let i = 0; i < p.length; i++) {
//     //     //     p[i].scrollTo(0, 0);
//     //     // }
//     //     // next();
//     // }

//     scrollToTop() {
//         window.scrollTo({
//             top: 0,
//             left: 0,
//             behavior: 'smooth'
//         });
//     }

//     // https://codepen.io/Mamboleoo/post/scrollbars-and-css-custom-properties

//     getScrollbarSize() {
//         // el elemento que genera el scroll, suele ser el root
//         const scroller1 = document.scrollingElement as any; // es el elemento html, pero este no tiene la scrollbar ??
//         const scroller = document.body;
//         // Force scrollbars to display, NOOOO
//         //scroller.style.setProperty('overflow', 'scroll');
//         // window.innerWidth, includes the width of the vertical scroll barm if its present
//         // scroller.clientWidth, inner width of the element in pixels, includes padding, but excludes, borders, margins and vertical scroll bar, if present
//         // console.log(
//         //     scroller,
//         //     document.documentElement.clientWidth,
//         //     scroller1.clientWidth,
//         //     window.innerWidth,
//         //     document.body.clientWidth,
//         //     document.body.scrollWidth
//         // );
//         //return window.innerWidth - scroller.clientWidth;
//         //return document.body.clientWidth - document.body.scrollWidth;
//         return window.innerWidth - document.documentElement.clientWidth;
//     }

//     setScrollbarSize() {
//         const scrollbarWidth = this.getScrollbarSize();
//         console.log('scrollbar width: ' + scrollbarWidth);
//         document.documentElement.style.setProperty('--scrollbar', `${scrollbarWidth}px`);
//         //console.log(document.documentElement.style.getPropertyValue('--scrollbar'));
//     }

//     checkOverflowers() {
//         document.querySelectorAll('*').forEach(el => {
//             // si son iguales, el contenido del elemento, cabe sin necesidad de scroll horizontal
//             if (el.clientWidth == el.scrollWidth) {
//                 //console.log('its ok');
//             } else {
//                 if (el.offsetWidth > document.documentElement.offsetWidth) {
//                     console.log('Found the worst element ever: ', el);
//                 }
//             }
//         });
//     }
// }
