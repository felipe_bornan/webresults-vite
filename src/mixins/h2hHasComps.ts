'use strict';

import { Component, Vue } from 'vue-property-decorator';
@Component({
    props: ['listCompetitor']
})
export default class H2HHasComps extends Vue {
    get hasComps(): boolean {
        const comps = this.$props.listCompetitor;
        if (!comps) return false;
        let val = false;
        for (let i = 0; i < comps.length; i++) {
            val = val || comps[i].Reg.length > 0;
        }
        return val;
    }
    // en algunos componentes no se llama listCompetitor
    // en este metodo paso el objeto como argumento
    hasCompsData(data: any): boolean {
        const comps = data;
        if (!comps) return false;
        let val = false;
        for (let i = 0; i < comps.length; i++) {
            val = val || comps[i].Reg.length > 0;
        }
        return val;
    }
}
