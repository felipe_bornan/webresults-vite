import Vue from 'vue';

export const ScrollHeader = {
    inserted: function(el: any, binding: any) {
        const target: any = document.getElementById('scroll');
        const f = function(evt: any) {
            if (binding.value(evt, el)) {
                target.removeEventListener('scroll', f);
            }
        };
        target.addEventListener('scroll', f);
    }
};

// Make it available globally.
// Vue.directive('scroll', ScrollHeader)
