'use strict';

import { Vue } from 'vue-property-decorator';

export default Vue.extend({
    data() {
        return {
            tooltipText: '',
            visibleTooltip: false,
            postXTooltip: '',
            postYTooltip: ''
        };
    },
    methods: {
        showTooltip(evt: any, position: any, text: any, isChooseSport = true as boolean) {
            const routername: any = this.$route.name;
            const widthElem = 40;
            const heightElem = 40;
            const transX = 23;
            let postX = 0;
            let postY = -15;

            this.tooltipText = text;
            if (evt.target.nodeName === 'A' && routername.indexOf('disc_') != -1) {
                postX = evt.pageX - evt.offsetX + evt.target.firstChild.clientWidth / 2;
                postY += evt.pageY - evt.offsetY + evt.target.clientHeight / 2;
            } else {
                postX = evt.pageX - evt.offsetX + evt.target.clientWidth / 2;
                postY += evt.pageY - evt.offsetY + evt.target.clientHeight / 2;
            }

            switch (position) {
                case 'top':
                    postX += -widthElem;
                    postY += -heightElem;
                    break;
                case 'bottom':
                    postX += -widthElem;
                    postY += heightElem;
                    break;
                case 'right':
                    postX = 60;
                    postY += 0;
                    break;
                case 'left':
                    if (!isChooseSport) {
                        postX = 50;
                        postY += 0;
                    } else {
                        postX = evt.pageX - 150;
                    }
                    break;
                default:
                    postX += -widthElem;
                    postY += -heightElem;
                    break;
            }
            this.postXTooltip = postX + 'px';
            this.postYTooltip = postY + 'px';
            this.visibleTooltip = true;
        },
        hideTooltip() {
            this.tooltipText = '';
            this.visibleTooltip = false;
        }
    }
});
