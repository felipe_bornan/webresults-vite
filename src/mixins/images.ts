'use strict';
/* eslint-disable @typescript-eslint/no-var-requires */
import { Vue } from 'vue-property-decorator';
import config from '@/settings/config';
const theme = config.general.champ;
//
// const discBGs = import.meta.glob(`../assets/${theme}/images/backgrounds/DISC/*.jpg`);
const pictosALl = import.meta.globEager(`../assets/${theme}/images/pictos/**/*.png`);
const avatars = import.meta.globEager(`../assets/common/images/*.jpg`);
const medals = import.meta.globEager(`../assets/${theme}/images/medals/*.png`);

const getFlags = () => {
    return import.meta.globEager(`../assets/common/images/flags-v2/*.png`);
};
//
export default Vue.extend({
    methods: {
        discBg(): string {
            let img: any;
            try {
                //console.log('discBg: ' + this.$route.params.disc + '.JPG');
                img = import(`../assets/${theme}/images/backgrounds/DISC/${this.$route.params.disc}.jpg`);
            } catch (e) {
                img = import(`../assets/${theme}/images/backgrounds/DISC/stadium.jpg`);
            }
            if (img) {
                img.then(res => {
                    return res.default;
                });
            }
            // return img.default;
        },
        discBgDisc(disc: any): string {
            console.log('getting images for ', disc);
            let img: any;
            try {
                img = import(`../assets/${theme}/images/backgrounds/DISC/${disc}.jpg`);
            } catch (e) {
                img = import(`../assets/${theme}/images/backgrounds/DISC/stadium.jpg`);
            }
            if (img) {
                img.then(res => {
                    console.log(res.default);
                    return res.default;
                });
            }
        },
        picto(disc: string): null | string {
            if (disc)
                try {
                    //console.log('picto: ' + disc + '.PNG');
                    return pictosALl[`@theme/images/pictos/full/${disc}.png`];
                } catch (e) {
                    return null;
                }
            return null;
        },
        pictoIcon(disc: string, color: string) {
            if (!disc) return null;
            let folder = 'dark';
            if (color == 'w') folder = 'white';
            else if (color == 'n') folder = 'full';
            try {
                //console.log('pictoIcon: ' + disc + '.PNG');
                return pictosAll[`@theme/images/pictos/${folder}/${disc}.png`];
            } catch (e) {
                return null;
            }
        },

        pictoSvg(disc: string) {
            if (!disc) return null;
            try {
                // console.log('pictoSvg: ' + disc + '.SVG');
                return pictosALl[`@theme/images/pictos/svg_250/${disc}.svg`];
            } catch (e) {
                return null;
            }
        },

        flag(org: string) {
            const flags = getFlags();
            // console.log('all Flags');
            // console.log(flags);
            if (org && org != 'BYE')
                try {
                    return flags[`../assets/common/images/flags-v2/${org}.png`].default;
                } catch (e) {
                    return null;
                }
            return null;
        },
        randomPhoto() {
            const min = Math.ceil(1);
            const max = Math.floor(70);
            const randomNumber = Math.floor(Math.random() * (max - min + 1) + min);
            return 'https://i.pravatar.cc/50?img=' + randomNumber;
        },
        defaultPhoto() {
            //return require('@/assets/images/avatar-small.jpg');
            return 'https://joeschmoe.io/api/v1/ben';
        },
        photo(reg: string) {
            // get image with FETCH
            // https://thewebdev.info/2021/09/25/how-to-use-the-fetch-api-to-get-an-image-from-a-url/
            if (!reg || reg == 'default' || config.general.photoPath == '') {
                return this.defaultPhoto();
            } else {
                //console.log('Busca foto para reg:', reg);
                const imageUrl = `${config.general.photoPath}${reg}.jpg`;
                return this.randomPhoto();
                // await fetch(imageUrl, { mode: 'no-cors' })
                //     .then(response => {
                //         //console.log(response);
                //         if (!response.ok) {
                //             //console.log('Network response was not OK');
                //             return this.defaultPhoto();
                //         }
                //         return imageUrl;
                //     })
                //     .catch(error => {
                //         //console.log('--ERROR: There has been a problem with your fetch operation:', error);
                //         return this.defaultPhoto();
                //     });
            }
        },
        errPhoto(event: any) {
            console.log('captura el error ??');
            event.target.src = avatars[`@theme/images/avatar.jpg`];
        },
        errPhotoSm(event: any) {
            event.target.src = avatars[`@theme/images/avatar-small.jpg`];
        },
        medals(user: any) {
            return this.getMedals(user.Medal);
        },

        getMedals(type: string) {
            switch (type) {
                case 'ME_GOLD':
                case '1':
                    return medals[`@theme/images/medals/ME_GOLD.png`];
                case 'ME_SILVER':
                    return medals[`@theme/images/medals/ME_SILVER.png`];
                case 'ME_BRONZE':
                case '2':
                    return medals[`@theme/images/medals/ME_BRONZE.png`];
            }
            return null;
        }
    }
});
