import Vue from 'vue';
import VueAnalytics from 'vue-analytics';
import i18n from '@/settings/i18n.vite';
// https://vue-scrollto.netlify.com/docs/#as-a-vue-directive
import VueScrollTo from 'vue-scrollto';
//
import router from './router';
import store from './store';
import config from './settings/config';
//
import App from './App.vue';
//bootstrap-vue
import { CollapsePlugin } from 'bootstrap-vue';
//
const champ = config.general.champ;
// componentes especificos del theme = champ
// const champComponents = import(`./assets/${champ}/components/components`);
const champComponents = import('@theme/components/components');
//
Vue.use(CollapsePlugin);

Vue.use(VueScrollTo);
Vue.config.productionTip = false;

if (config.analytics.enable) {
    Vue.use(VueAnalytics, {
        id: config.analytics.id,
        router,
        debug: {
            enabled: config.analytics.debug,
            sendHitTask: !config.analytics.debug
        }
    });
}

// champ COMPONENTS
if (champComponents) {
    // console.log('champComps');
    // console.log(champComponents);
    champComponents
        .then(comps => {
            // console.log('in components: ', comps.default);
            Object.entries(comps.default).forEach(([key, component]) => {
                //console.log(typeof component);
                // console.log('registering COMPONENT: ' + key);
                // es una PRomise devuelta por importar dinamicamente
                if (typeof component == 'object') {
                    // console.log('promise COMPONENT: ' + key);
                    component.then(comp => {
                        // console.log(comp.default);
                        // componentes importados dinamicamente
                        Vue.component(key as string, comp.default);
                    });
                } else {
                    // console.log('imported as normal ' + key);
                    // componentes importados normalmente
                    Vue.component(key as string, component);
                }
            });
        })
        .catch(error => {
            console.log(error);
        });
}

router.afterEach((to, from) => {
    const scrolling = document.scrollingElement;
    // casi siempre el elmento que hace scroll es el HTML == document.documenElement
    // funciona incluso el Safari 12.1 del iPhone6
    if (window) {
        setTimeout(() => {
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }, 100);
    }
    // en iOS no existe este elemento ? auqnue el scroll funciona
    if (scrolling) {
        setTimeout(() => {
            scrolling.scrollTop = 0;
        }, 200);
    }
    // setTimeout(() => {
    //     const html = document.documentElement as HTMLElement;
    //     const box = html.getBoundingClientRect();
    //     console.log('html elemento values:');
    //     console.log(box);
    //     console.log(
    //         'scrollTop value = ',
    //         html.scrollHeight,
    //         '-',
    //         window.innerHeight,
    //         ' = ',
    //         html.scrollHeight - window.innerHeight
    //     );
    // }, 500);
});

new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount('#app');
