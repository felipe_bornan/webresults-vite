// COMPONENTES propios del theme, importados normalmente
import IconSet from './IconSet.vue';
const components = { IconSet };
// DISCIPLINES
const disciplines = [
    'ARC',
    'ATH',
    'BDM',
    'BHB',
    'BK3',
    'BOX',
    'CLB',
    'CRD',
    'FEN',
    'FSK',
    'FSL',
    'GAR',
    'HOC',
    'JUD',
    'KTE',
    'RU7',
    'SKB',
    'SSK',
    'SWM',
    'TEN',
    'TKW',
    'TRI',
    'TTE',
    'VBV',
    'WLF',
    'WRE'
];

// COMPONENTES de las disciplinas, importados dinamicamente
const discComponents: { [key: string]: any } = {};
disciplines.forEach(disc => {
    discComponents[`Unit${disc}`] = import(`@/components/DisciplinesMatchCentre/${disc}/ScheduleUnit.vue`);
    discComponents[`Presentation${disc}`] = import(`@/components/DisciplinesMatchCentre/${disc}/Presentation.vue`);
    discComponents[`Start${disc}`] = import(`@/components/DisciplinesMatchCentre/${disc}/StartList.vue`);
    discComponents[`Res${disc}`] = import(`@/components/DisciplinesMatchCentre/${disc}/Results.vue`);
});

// const discComponents: { [key: string]: any } = {};
// const promiseComponents = Promise.all(Object.values(discPromises)).then(values => {
//     values.forEach((component, index) => {
//         discComponents[Object.keys(discPromises)[index]] = component;
//     });
//     return { ...components, ...discComponents };
// });

export default { components, discComponents };
