import Vue from 'vue';
import Vuex from 'vuex';
import moment from 'moment';
import pako from 'pako';
import services from '@/settings/services';
import config from '@/settings/config';

Vue.use(Vuex);

const champ = config.general.champ;
const lsKeys = {
    lang: `${champ}_language`,
    myTime: `myTime`,
    panelA: 'panel',
    date: `${champ}_date`,
    list: `${champ}_disciplines`,
    events: `${champ}_disc_events`,
    data: `${champ}_disc_data`,
    sch: `${champ}_schedule_days`
};

function hexEncode(value: string) {
    let hex = '';
    let result = '';
    for (let i = 0; i < value.length; i++) {
        hex = value.charCodeAt(i).toString(16);
        result += ('000' + hex).slice(-4);
    }
    return result;
}
function hexDecode(value: string) {
    const hexes = value.match(/.{1,4}/g) || [];
    let back = '';
    for (let j = 0; j < hexes.length; j++) {
        back += String.fromCharCode(parseInt(hexes[j], 16));
    }
    return back;
}

function fromStore(key: string, init: any, isObject: boolean, gzip = true): string | any {
    let value = null;
    const lmzaHexString = localStorage.getItem(key);
    if (gzip && lmzaHexString) {
        const lmzaString = hexDecode(lmzaHexString);
        if (lmzaString) value = pako.inflate(lmzaString, { to: 'string' });
    } else value = lmzaHexString;
    if (value && isObject) {
        try {
            value = JSON.parse(value);
        } catch (err) {
            console.error((err as any).message);
            value = null;
        }
    }
    if (!value) {
        toStore(key, init, isObject, gzip);
    }
    return value ? value : init;
}

function toStore(key: string, value: any, isObject: boolean, gzip = true) {
    if (isObject) value = JSON.stringify(value);
    if (gzip == true) {
        const lmzaString = pako.deflate(value, { to: 'string' });
        const lmzaHexString = hexEncode(lmzaString);
        localStorage.setItem(key, lmzaHexString);
    } else localStorage.setItem(key, value);
}

function cleanStore() {
    localStorage.removeItem(lsKeys.date);
    localStorage.removeItem(lsKeys.list);
    localStorage.removeItem(lsKeys.events);
    localStorage.removeItem(lsKeys.data);
    localStorage.removeItem(lsKeys.sch);
}

function hasToReload(state: any): boolean {
    const now = Date.now();
    const date = state.date || 0;
    const version = config.general.storage ? new Date(config.general.storage).getTime() : false;
    //let reloadStorage = (now - date) > (1000 * 3600 * 24)
    //reloadStorage = reloadStorage || (version!=false && (version - date)>0 )
    const reloadStorage = version !== false && version - date > 0;
    return reloadStorage;
}

const appStore = new Vuex.Store({
    state: {
        languages: [
            {
                Key: 'en',
                Desc: 'English'
            }
        ],
        language: 'en',
        myTime: 'false',
        disciplines: [],
        events: {},
        buttons: {},
        scheduleDays: {},
        panelA: '',
        date: 0,
        sponsors: [],
        // NavBars
        leftOpen: false,
        discOpen: false
    },
    getters: {
        champDesc(state: any): string {
            let langIdx = state.languages.findIndex((el: any) => el.Key === state.language) + 1;
            if (langIdx === 0) langIdx = 1;
            return config.general.champDesc[('S' + langIdx) as 'L1'];
        },
        languages(state: any): Array<any> {
            console.log('getting langs', state.language);
            moment.locale(state.language);
            return state.languages;
        },
        language(state: any): string {
            moment.locale(state.language);
            return state.language;
        },
        myTime(state: any): boolean {
            return state.myTime == 'true';
        },
        disciplines(state: any): any[] {
            return state.disciplines;
        },
        events(state: any) {
            return state.events;
        },
        buttons(state: any) {
            return state.buttons;
        },
        scheduleDays(state: any) {
            return state.scheduleDays;
        },
        panelA(state: any) {
            return state.panelA;
        }
    },
    mutations: {
        INIT: (state: any) => {
            state.date = Number(fromStore(lsKeys.date, 0, false, false));
            state.language = fromStore(lsKeys.lang, 'en', false, false);
            state.myTime = fromStore(lsKeys.myTime, 'false', false, false);
            state.panelA = fromStore(lsKeys.panelA, 0, true, false);
            state.disciplines = fromStore(lsKeys.list, [], true);
            state.events = fromStore(lsKeys.events, {}, true);
            state.buttons = fromStore(lsKeys.data, {}, true);
            state.scheduleDays = fromStore(lsKeys.sch, {}, true);
        },
        SET_DATA: (state: any, payload: any) => {
            state.date = payload.date;
            state.scheduleDays = payload.scheduleDays;
            state.disciplines = payload.disciplines;
            state.events = payload.events;
            state.buttons = payload.buttons;
        },
        //
        sponsors: (state: any, payload: any[]) => {
            if (Array.isArray(payload)) state.sponsors = payload;
            else state.sponsors = [];
        },
        //
        //
        closeLeft: (state: any) => {
            state.leftOpen = false;
        },
        toggleLeft: (state: any) => {
            state.leftOpen = !state.leftOpen;
        },
        toggleDisc: (state: any) => {
            state.discOpen = !state.discOpen;
        },
        //
        //
        //
        //
        SET_LANGS: (state: any, languages: any[]) => {
            state.languages = [];
            for (let i = 0; i < languages.length; i++) {
                state.languages.push({
                    Key: languages[i].lang,
                    Desc: languages[i].native
                });
            }
        },
        CHANGE_LANG: (state: any, language: any) => {
            moment.locale(language);
            state.language = language;
            toStore(lsKeys.lang, language, false, false);
            cleanStore();
            window.location.reload();
        },
        CHANGE_TIME: (state: any, myTime: boolean) => {
            state.myTime = myTime === true ? 'true' : 'false';
            toStore(lsKeys.myTime, state.myTime, false, false);
        },
        DISC_DATA: (state: any, data: any) => {
            const disc = data.Disc;
            state.events[disc] = data.Events;
            //
            delete data.Events;
            delete data.DiscDesc;
            state.buttons[disc] = data;
            //
            toStore(lsKeys.events, state.events, true);
            toStore(lsKeys.data, state.buttons, true);
        },
        PANELA_COUNT: (state: any) => {
            state.panelA += 1;
            toStore(lsKeys.panelA, state.panelA, true, false);
        }
    },
    actions: {
        update: async context => {
            if (!hasToReload(context.state)) {
                return;
            }
            cleanStore();
            let hasErrors = false;
            const stateDisc = context.state.disciplines;
            //
            // Disciplines
            let disciplines: any[] = await services.link('disciplines');
            if (disciplines == null) {
                disciplines = stateDisc;
            }
            toStore(lsKeys.list, disciplines, true);

            //
            // Events & Data
            const eventsDisc: any = {};
            const dataDisc: any = {};
            for (let i = 0; i < disciplines.length; i++) {
                const disc = disciplines[i].Key;
                const data = await services.link('disc_data', disc);
                hasErrors = hasErrors || data == null;
                eventsDisc[disc] = data ? data.Events : [];
                if (data) {
                    delete data.Events;
                    delete data.DiscDesc;
                }
                dataDisc[disc] = data ? data : {};
            }
            toStore(lsKeys.events, eventsDisc, true);
            toStore(lsKeys.data, dataDisc, true);

            //
            // Disciplines days!
            const daysDisc: any = {};
            let listDisc: Array<any>;
            for (let i = 0; i < disciplines.length; i++) {
                listDisc = await services.link('schedule_days', disciplines[i].Key);
                if (listDisc != null) {
                    daysDisc[disciplines[i].Key] = listDisc;
                } else {
                    hasErrors = true;
                }
            }
            toStore(lsKeys.sch, daysDisc, true);

            // save time of construction
            const date = hasErrors ? 0 : Date.now();
            toStore(lsKeys.date, date.toString(), false, false);

            context.commit('SET_DATA', {
                date,
                scheduleDays: daysDisc,
                disciplines: disciplines,
                events: eventsDisc,
                buttons: dataDisc
            });
        }
    }
});

export default appStore;

/* Check Size of localStorage
var _lsTotal=0,_xLen,_x;
for(_x in localStorage){
    if(!localStorage.hasOwnProperty(_x)){continue;}
    _xLen= ((localStorage[_x].length + _x.length)* 2);
    _lsTotal+=_xLen;
    console.log(_x.substr(0,50)+" = "+ (_xLen/1024).toFixed(2)+" KB")
};
console.log("Total = " + (_lsTotal / 1024).toFixed(2) + " KB");
*/

/* var paco = new Vuex.Store({
paco.watch((state:any) => state.language, (oldValue:any, newValue:any) => {
  console.log('search string is changing')
  console.log(oldValue)
  console.log(newValue)
})
export default paco */
